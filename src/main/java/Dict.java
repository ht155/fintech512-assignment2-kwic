import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class Dict {
    private HashMap<String, ArrayList<Title>> dict  = new HashMap<String, ArrayList<Title>>();
    private Inputs inputs;
    Dict(Inputs inputs){
        ArrayList<Title> titles = inputs.getTitles();
        this.inputs = inputs;
        for(String keyword : inputs.getKeywords()){
            ArrayList<Title> tl =  new ArrayList<Title>();
            for(Title temp : titles){
                if(temp.getTitle().contains(keyword)){
                    tl.add(temp);
                }
            }
//            tl.sort(new Comparator<Title>() {
//                @Override
//                public int compare(Title o1, Title o2) {
//                  return  o1.getTitle().compareTo(o2.getTitle());
//                }
//            });
            dict.put(keyword, tl);
        }
    }

    public void print(){
        for(String key : inputs.getKeywords()){
            for(Title title : dict.get(key)){
                //System.out.println(key);
                title.print(key);
            }
        }
    }
}
