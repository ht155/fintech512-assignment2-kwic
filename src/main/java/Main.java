
public class Main {
    public static void main(String args[]){
        Inputs inputs =  new Inputs();
        inputs.read();
        Dict dict = new Dict(inputs);
        dict.print();
    }
}
