import java.util.Locale;

public class Title {
    private String title;
    Title(String title){
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void print(String keyword){
        keyword = keyword.toLowerCase(Locale.ROOT);
        if(title.indexOf(keyword) == -1){
            System.out.println(this.title + " does not contain keyword \"" + keyword + "\"");
        }
        else{
            String temp = this.title;
            //temp = temp.replace(keyword, keyword.toUpperCase(Locale.ROOT));
            int index = -1;
            while((index = temp.indexOf(keyword, index + 1) ) != -1){
                StringBuilder sb = new StringBuilder(temp);
                sb.replace(index, index + keyword.length(), keyword.toUpperCase(Locale.ROOT));
                System.out.println(sb);
            }
        }
    }

}
