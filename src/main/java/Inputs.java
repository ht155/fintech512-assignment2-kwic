import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;
import java.util.Scanner;

public class Inputs {
    private final ArrayList<Title> titles = new ArrayList<Title>();
    private final ArrayList<String> ignored = new ArrayList<String>();
    private final ArrayList<String> keywords = new ArrayList<String>();

    public void read(){
        Scanner scan = new Scanner(System.in);
        boolean isTitle = false;
        while(scan.hasNextLine()){
            String str = scan.nextLine();
            if(str.length() == 0){
                continue;
            }
            if(str.equals("::")){
                isTitle = true;
                continue;
            }
            if(!isTitle){
                ignored.add(str);
            }
            else{
                Title title = new Title(str.toLowerCase(Locale.ROOT));
                titles.add(title);
                parseKeywords(title.getTitle());
            }
        }
        sort();
    }

    public ArrayList<String> getIgnored(){
        return ignored;
    }

    public ArrayList<String> getKeywords(){
        return keywords;
    }

    public ArrayList<Title> getTitles(){
        return titles;
    }

    public void sort(){
        keywords.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
    }

    public void parseKeywords(String title) {
        String[] words = title.split(" ");
        for (String word : words) {
            if (!ignored.contains(word) && !keywords.contains(word)) {
                keywords.add(word);
            }
        }
    }
}
