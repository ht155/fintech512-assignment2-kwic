import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

class InputsTest {

    @Test
    void readKeyWords() {
        String str = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        ByteArrayInputStream strIn = new ByteArrayInputStream(str.getBytes());
        Inputs inputs = new Inputs();
        System.setIn(strIn);
        inputs.read();
        System.out.println("ignored:");
        for(String s : inputs.getIgnored()){
            System.out.println(s);
        }

        System.out.println("Titles:");
        for(Title t : inputs.getTitles()){
            System.out.println(t.getTitle());
        }

        System.out.println("Keywords:");
        for(String s : inputs.getKeywords()){
            System.out.println(s);
        }

        inputs.sort();
        System.out.println();
        System.out.println("Sorted Keywords:");
        for(String s : inputs.getKeywords()){
            System.out.println(s);
        }
    }
}