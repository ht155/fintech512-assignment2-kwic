import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

import static org.junit.jupiter.api.Assertions.*;

class DictTest {

    @Test
    void print() {
        String str = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        ByteArrayInputStream strIn = new ByteArrayInputStream(str.getBytes());
        Inputs inputs = new Inputs();
        System.setIn(strIn);
        inputs.read();
        Dict dict = new Dict(inputs);
        dict.print();
    }
}