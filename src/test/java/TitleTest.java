import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class TitleTest {

    @Test
    void getTitle() {
        Title title = new Title("apple pie");
        assertEquals("apple pie", title.getTitle());
    }

    @Test
    void print(){
        Title title = new Title("apple pie apple pie");
        title.print("pie");
    }
}